#!/bin/bash

set -e
set -x

rm -R /etc/init.d/* 2>/dev/null

mkdir /etc/pam.d

sed -r -n -i '/session(\s+)required(\s+)pam_loginuid/{s|^|#|};p'  /etc/pam.d/login
